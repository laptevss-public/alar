[![pipeline status](https://gitlab.com/laptevss-public/alar/badges/master/pipeline.svg)](https://gitlab.com/laptevss-public/alar/-/commits/master)

# AWS

## Схема работы:

- задаем переменные AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION
- указываем vpc-id (`aws_ec2.yaml`, `playbook.yml`)
- указываем ansible_ssh_private_key_file (`playbook.yml`)
- в `group_vars/instances.yml` указываем необходимое количество инстансов app и web в каждой подсети (subnet)
- запускаем playbook
- playbook приводит количество инстансов к указанному
- устанавливает nginx
- генерирует конфигурационный файл nginx, указав в upstream серверы app из одной зоны доступности (AZ)
- копирует конфигурации nginx на серверы web и применяет конфигурацию

## Запуск

```
ansible-galaxy collection install -r requirements.yml
ansible-playbook playbook.yml  --inventory aws_ec2.yaml --inventory localhost.yaml
```

## Проверить inventory

```
ansible-inventory --inventory aws_ec2.yaml --inventory localhost.yaml --graph --yaml
```

## Проверить inventory и переменные

```
ansible-inventory --inventory aws_ec2.yaml --inventory localhost.yaml --list --yaml
```
